<?php
/**
 * 
 * Template: Landing
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _sf v1.1.1
 * @ package htl
 * @since htl 0.1
 */
get_header(); 
$sidebar = get_theme_mod('_sf_default_sidebar');
_sf_open($sidebar);
?>
	
			
<?php while ( have_posts() ) : the_post(); ?>
	<?php do_action( 'tha_entry_before' ); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class('landing-page'); ?>>
		<?php do_action( 'tha_entry_top' ); ?>
			<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<div class="row" id="orbit-row">
					<div class="large-12 columns">
						<?php get_template_part('landing', 'slider'); ?>
					</div>
				</div>
				
				<div class="row" id="landing-cta-row">
					<div class="large-8 small-12 columns large-centered " id="landing-cta">
						<h3>Get Involved</h3>
						<ul>
							<li><a href="<?php echo get_site_url(3); ?>" title="Devlopment Blog">Get Involved In The Development of The Holo Tree</a></li>
							<li><a href="<?php echo get_site_url(2); ?>" title="Josh's Detailed Proposal">Read The Detailed Proposal For The Holo Tree</a></li>
							<li>Connect: <?php ht_social(); ?></li>
						</ul>
						<br />
						<a href="#"class="button radius" id="landing-cta-button alignright">Join!</a>
					</div>
				</div>
				
			</div><!-- .entry-content -->
			<?php edit_post_link( __( 'Edit', '_sf' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
		<?php do_action( 'tha_entry_bottom' ); ?>
		</article><!-- #post-## -->
		<?php do_action( 'tha_entry_after' ); ?>
	

<?php endwhile; ?>

<?php _sf_close($sidebar); ?>
