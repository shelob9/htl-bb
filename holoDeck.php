<?php
/**
*
 * Template Name: holo deck
 * @package (page.php/ conent-page.php) _sf v1.1.1
 * @package htl
 * @since 0.1
 */
get_header(); 
$sidebar = get_theme_mod('_sf_default_sidebar');
_sf_open($sidebar);
?>
	
<?php do_action( 'tha_entry_before' ); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('holotree'); ?>>
<?php do_action( 'tha_entry_top' ); ?>
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<div class="row">
			<div class="large-12 columns">
				<h3>Books</h3>
				<p>The Holo Deck will be the book club of The Holo Tree. For now, enjoy this list of recommended books.</p>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<?php htl_holo_deck(); ?>
			</div>
		</div>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', '_sf' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
<?php do_action( 'tha_entry_bottom' ); ?>
</article><!-- #post-## -->
<?php do_action( 'tha_entry_after' ); ?>

<?php _sf_close($sidebar); ?>
