<?php
/**
* I am a good place to put new functions for the child theme.
*/

/**
* Live reload, reloading script
* DO NOT ALLOW THIS TO GO LIVE
*/
if (! function_exists('_sf_live_reload') && ! is_admin() ) :
function _sf_live_reload() { ?>
	<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<?php 
}
add_action('tha_body_bottom', '_sf_live_reload');
endif;

/**
* CC License Block
*
* @ since htl 0.1
*/
if (! function_exists('htl_social_cc_row') ):
function htl_social_cc_row() { 
	echo '<div class="row" id="social-cc-row">';
	if ( is_front_page() ) {
		echo '<div class="large-4 columns large-centered small-6">';
	}
	else {
		echo '<div class="large-4 large-offset-2 columns small-6">';
		//bccl_license_block();
		echo 'All content, unless otherwise expressly stated, is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a>.</p>';
	}
	echo '</div>';
	if (! is_front_page() ) {
		echo '<div class="large-6 columns">';
		ht_social();
	}
	echo '</div><!--/#social-cc-row-->';
	/* Background photo by <a href="http://no.wikipedia.org/wiki/Bruker:Torvol">Torvol</a> and is part of the <a href="http://commons.wikimedia.org/wiki/File:Andoya3.JPG">wiki commons</a>.*/
}
add_action('tha_footer_bottom', 'htl_social_cc_row');
endif; //! function_exists

/**
* Holo Tree Social Links
*
*DUDE- replace this with a plugin, with settings and shit
* @since htl 0.1
*/
if (! function_exists('ht_social') ):
function ht_social() { ?>
				<a href="https://twitter.com/holotree" class="genericon genericon-twitter" title="Holo Tree Twitter"></a>
				<a href="https://plus.google.com/u/0/b/114785578052502988633/114785578052502988633/" class="genericon genericon-googleplus" title="Holo Tree Google+"></a>
				<a href="mailto:JPollock412@gmail.com" class="genericon genericon-mail"></a>
				<a href="https://github.com/organizations/Holo-Tree" class="genericon genericon-github" title="Holo Tree Source Code on Github"></a>
				<a href="https://www.facebook.com/HoloTree" class="genericon genericon-facebook" title="Holo Tree Facebook"></a>
<?php
}
endif; //!function_exists

/**
* Reading List For HoloDeck
* 
* DUDE- This should be generated from CPTs
*
* @since htl 0.1
*/
if (! function_exists('htl_holo_deck') ) :
function htl_holo_deck() {
	$holodeck[] = array(
		'title' 		=> 'Sacred Economics',
		'author' 		=> 'Charles Eisenstein',
		'description' 	=> 'Lorem ipsums.',
		'booklink' 		=> 'http://www.indiebound.org/book/9781583943977',
		'authorlink'	=> 'http://charleseisenstein.net/',
		'coverimg'		=> 'http://sacred-economics.com/wp-content/images/icon-sacred.jpg',
	);
	$holodeck[] = array(
		'title' 		=> 'Symbiotic Planet',
		'author' 		=> 'Lynn Margulis',
		'description' 	=> '',
		'booklink' 		=> 'http://www.amazon.com/Symbiotic-Planet-New-Look-Evolution/dp/0465072720/ref=sr_1_1?s=books&ie=UTF8&qid=1375688911&sr=1-1&keywords=symbiotic+planet',
		'authorlink'	=> '',
		'coverimg'		=> 10,
	);
	
	$holodeck[] = array(
		'title' 		=> 'The Biophila Hypothesis',
		'author' 		=> 'Stephen Kellert and E.O. Wilson (editors)',
		'description' 	=> '',
		'booklink' 		=> 'http://www.amazon.com/Biophilia-Hypothesis-Shearwater-book/dp/1559631473/ref=sr_1_1?s=books&ie=UTF8&qid=1375689019&sr=1-1&keywords=the+biophilia+hypothesis',
		'authorlink'	=> '',
		'coverimg'		=> 'http://www.wilderdom.com/images/evolution/biophilia.jpg',
	);
	$holodeck[] = array(
		'title' 		=> 'Owning Our Future',
		'author' 		=> 'Marjorie Kelly',
		'description' 	=> '',
		'booklink' 		=> 'http://www.indiebound.org/book/9781605093109',
		'authorlink'	=> 'http://www.marjoriekelly.com/',
		'coverimg'		=> 'http://www.marjoriekelly.com/wp-content/uploads/2009/09/Cover-image-198x300.jpg',
	);
	$holodeck[] = array(
		'title' 		=> 'The Spirit Level',
		'author' 		=> 'Richard Wilkinson and Kate Picket',
		'description' 	=> '',
		'booklink' 		=> 'http://www.amazon.com/Spirit-Level-Equality-Societies-Stronger/dp/1608193411/ref=sr_1_1?s=books&ie=UTF8&qid=1375688955&sr=1-1&keywords=The+Spirit+Level',
		'authorlink'	=> '',
		'coverimg'		=> 'http://upload.wikimedia.org/wikipedia/en/thumb/8/85/The-spirit-level-bookcover.jpg/200px-The-spirit-level-bookcover.jpg',
	);
	$holodeck[] = array(
		'title' 		=> 'Sustainability By Design',
		'author' 		=> 'John Ehrenfeld',
		'description' 	=> '',
		'booklink' 		=> 'http://www.amazon.com/Sustainability-Design-Subversive-Strategy-Transforming/dp/0300158432/ref=sr_1_1?s=books&ie=UTF8&qid=1375689129&sr=1-1&keywords=sustainability+by+design',
		'authorlink'	=> '',
		'coverimg'		=> 'http://ecx.images-amazon.com/images/I/514ZDuXq91L.jpg',
	);
	$holodeck[] = array(
		'title' 		=> 'What Is Life?',
		'author' 		=> 'Lynn Margulis and Dorian Sagan',
		'description' 	=> '',
		'booklink' 		=> 'http://www.amazon.com/What-Life-Lynn-Margulis/dp/0520220218/ref=sr_1_1?s=books&ie=UTF8&qid=1375689162&sr=1-1&keywords=what+is+life%3F+margulis+sagan',
		'authorlink'	=> '',
		'coverimg'		=> 'http://ecx.images-amazon.com/images/I/4107JGJ0KVL._SY300_.jpg',
	);
	$holodeck[] = array(
		'title' 		=> 'The Political Mind',
		'author' 		=> 'George Lakoff',
		'description' 	=> '',
		'booklink' 		=> 'http://www.indiebound.org/book/9780143115687',
		'authorlink'	=> '',
		'coverimg'		=> 'http://images.indiebound.com/687/115/9780143115687.jpg',
	);
	
	foreach ($holodeck as $book) {
		if (! is_string($book['coverimg'])) {
			$src = wp_get_attachment_image_src($book['coverimg'], 'medium');
			$cover = $src[0];
		}
		else {
			$cover = $book['coverimg'];
		}
		if ($book['booklink'] != '' ) {
			$title = '<a href="'.$book['booklink'].'" target="_blank">'.$book['title'].'</a>';
		}
		else {
			$title = $book['title'];
		}
		if ($book['authorlink'] != '') {
			$author = '<a href="'.$book['authorlink'].'" target="_blank">'.$book['author'].'</a>';
		}
		else {
			$author = $book['author'];
		}
		echo '<div class="row holo-deck-entry">';
		echo '<div class="large-3 columns holo-deck-cover">';
		echo '<img src="'.$cover.'" alt="Cover of '.$book['title'].'" />';
		echo '</div>';
		echo '<div class="large-9 columns holo-deck-info">';
		echo '<h5>'.$title.'<h5>';
		echo '<h7>'.$author.'</h7>';
		echo '<p>'.$book['description'];
		echo '</div></div>';
	}
		
}
endif;

?>