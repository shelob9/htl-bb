<!-- Orbit Slides -->
<!--Dude- try and keep this to 8 total-->
<div class="slideshow-wrapper">
	<div class="preloader"></div>
	<ul data-orbit>      
		<li>
			<div class="row">
				<div class="large-12 columns">
					<h3 class="slide-title">What Is The Holo Tree?<h3>
				</div>
			</div>
			<div class="row">
				<div class="large-12 columns">
					<p>A social networking tool for crowdfunding sustainable design. This tool will ask users what they want to do to make the world better and then systematically connects them with members of the Holo Tree and their other social networks that can help them accomplish these goals. </p>
					<strong>Put the global thing here</strong>
				</div>
			</div>
		</li>
		<li>
			<div class="row">
				<div class="large-12 columns">
					<h3 class="slide-title">Open Source Sustainability<h3>
				</div>
			</div>
			<div class="row">
				<div class="large-12 columns">
					<blockquote>
						<p>Community is woven from gifts. Unlike money or barter transactions, in which there are no obligations remaining after the transaction, gifts always imply future gifts. When we receive, we owe; gratitude is the knowledge of having received and the desire to give in turn.</p>
						<cite><a title="Charles Eisenstien" href="http://charleseisenstein.net/">Charles Eisenstein,</a> <a title="Sacred Economics" href="http://Scared-Economics.com" target="_blank">Sacred Economics</a></cite>
					</blockquote>
						<h3>The Holo Tree is being built on open-source software and is committed to the idea that the transition to a sustainable economy will require a strengthening of local economies and and a new commitment to sharing.</h3> 
						<h5>This is globalization without the homogenization.</h5>
				</div>
			</div>
		</li>
		<li>
		
			<div class="row">
				<div class="large-12 columns">
				<blockquote>
					<p>If you want to accomplish something in the world, idealism is not enough—you need to choose a method that works to achieve the goal. In other words, you need to be “pragmatic.” </p>
					<cite>Richard Stallman</cite>
				</blockquote>
				<h3>The goal of sustainable design must be to create systems that people will adopt because they are practical and beneficial to themselves.</h3>
				<h5>This isn't about trying to change people, it's about changing the way we make money.</h5>
				<h5>It's not about making things that use less energy, but changing the way we make things.</h5> 
				<p>If Holo Tree is about one thing, then it is about providing a way for people who have the impetus to do something good for the world with their careers to earn a decent living through right livelihood. For college students today, facing a stagnating and/or shrinking job market Holo Tree will represent a way to create a job for themselves and those in their social networks instead of competing against those people for a shrinking number of jobs. I am ending with this example because this is very much what Holo Tree is all about: creating the pathways to the world that we want to see in the future.</p>
				</div>
			</div>
		</li>
		<li>
			<div class="row">
				<div class="large-12 columns">
					<h3 class="slide-title">Ecologically Inspired Design<h3>
				</div>
			</div>
			<div class="row">
				<div class="large-12 columns">
					<p>Traditionally we’ve defined social Darwinism as dog-eat-dog competition, but dogs don’t eat other dogs. Dogs co-evolved with changes in human culture to become an important part of our society. Biological Darwinism shows us that species survive through out-competing other species, or reshaping their environment to better suit them, or by creating long-term symbiotic relationships with other species. For too long capitalism has been based only on the strategy of competition, ignoring the power of creating new environments - something humans are particularly good at - or forming symbiosis, which is a step employed whenever the quantum leaps of evolution occurred.</p>
				</div>
			</div>
		</li>
		<li>
			<div class="row">
				<div class="large-12 columns">
					<h3 class="slide-title">Without Leaders<h3>
				</div>
			</div>
			<div class="row">
				<div class="large-12 columns">
					<h3>What if we didn't 
					<h5>A commitment to a world where everyone's voice is heard.</h5>
				</div>
			</div>
		</li>
	</ul>
</div>
